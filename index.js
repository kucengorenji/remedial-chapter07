const express = require('express');
const app = express();
const { PORT = 3000 } = process.env;
const route = require('./routes/index');
const morgan = require('morgan');

app.use(morgan('tiny'));
app.use(express.json());
app.use(route);

app.listen(PORT, () => {
  console.log(`server is running now listening on http://localhost:${PORT}`);
});
