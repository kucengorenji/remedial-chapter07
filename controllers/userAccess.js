const { User, Group, UserGroup } = require('../models');

module.exports = {
  getAllGroup: async (req, res) => {
    const all = await UserGroup.findAll();
    return res.status(200).json(all);
  },
  getUserGroup: async (req, res) => {},
  deleteUserGroup: async (req, res) => {},
  updateUserGroup: async (req, res) => {},
  addUserGroup: async (req, res) => {},
  createGroup: async (req, res) => {},
};
