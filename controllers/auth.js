const { User, Group } = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {
  handleRegister: async (req, res) => {
    const { email, password } = req.body;
    const user = await User.create({
      email: email,
      password: bcrypt.hashSync(password, 10),
    });
    return res.status(201).json({ user });
  },
  handleLogin: async (req, res) => {
    const { email, password } = req.body;
    const user = await User.findOne({
      where: { email: email },
    });
    if (!bcrypt.compareSync(password, user.password)) {
      return res.status(403).json({
        message: `password anda salah`,
      });
    }
    const access_token = jwt.sign(
      { id: user.id, role: user.role, email: user.email },
      'RAHASIA',
      {
        expiresIn: '2h',
      }
    );
    const refresh_token = jwt.sign(
      { id: user.id, email: user.email, role: user.role, use: 'REFRESH_TOKEN' },
      'RAHASIA'
    );

    return res.status(201).json({ access_token, refresh_token });
  },
  who: async (req, res) => {
    const currentUser = req.user;
    return res.json(currentUser);
  },
};
