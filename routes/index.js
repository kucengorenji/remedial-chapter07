const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth');
const userController = require('../controllers/userAccess');
const authMiddleware = require('../middlewares/index');

// root & all group
router.get('/v1/group', userController.getAllGroup);

// user Control & access
// router.get('/v1/group/:id', userController.getUserGroup);
// router.delete('/v1/group/:id', userController.deleteUserGroup);
// router.put('/v1/group/:id', userController.updateUserGroup);
// router.post('/v1/group/:id', userController.addUserGroup);
// router.post(
//   '/v1/group/create',
//   authController.isAdmin,
//   userController.createGroup
// );

// auth
router.post('/v1/register', authController.handleRegister);
router.post('/v1/login', authController.handleLogin);
router.get('/v1/who', authMiddleware.verifyToken, authController.who);

module.exports = router;
